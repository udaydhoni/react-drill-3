
function Message (props) {
  console.log(props)
    return (
        <div className='layout'>
      <img src='https://images.unsplash.com/photo-1504512925757-05a29947cfdd?ixlib=rb-4.0.3&
      ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTF8fGJsdWUlMjBmbG93ZXJzfGVufDB8fDB8fA%3D%3D&auto=
      format&fit=crop&w=500&q=60'></img>
      <div>
      <div className='header-content'>
        
        <div className='header-status'>
          <p className='box'>{props.data['signupPage']}</p>
          <p>Sign Up</p>
        </div>
        <div className='header-status'>
          <p className='box'>{props.data['messagePage']}</p>
          <p>Message</p>
        </div>
        <div className='header-status'>
          <p className='box'>{props.data['checkboxPage']}</p>
          <p>Checkbox</p>
        </div>
        
      </div> 
        <hr></hr>
        <div className='message'>
          <div>
          <p>Step 2/3</p>
            <h1>Message</h1>
            <p>Message</p>
            <textarea rows='8' id='message' onChange={props.updater} value={props.data['message']}></textarea>
            <p className="error">{props.data['error']['message']}</p>
          </div>
            
            
            
            <br></br>
            <br></br>
            <div  id='radio-options'>
                <div className="options">
                <div>
                    <input type='radio' id='first-option' name='choice' onChange={props.updater} checked={props.data['first-option'] }></input>
                    <label htmlFor='first-option'>The number one choice</label>
                </div>
                
                <br></br>
                <br></br>
                <div>
                    <input type='radio' id='second-option' name='choice' onChange={props.updater} checked={props.data['second-option']}></input>
                    <label htmlFor='second-option'>The number two choice</label>
                </div>
                </div>
                <p className="error">{props.data['error']['radio']}</p>
                
            </div>
        <hr></hr>
        <br></br>
        <br></br>
        <div className="message-footer">
            <div className="message-back">
                <button onClick={props.back}>Back</button>
            </div>
            <div className='message-next'> 
                <button onClick={props.next}>Next Step</button>
            </div>
            
        </div>
        
      </div>
    </div>
    </div>
    )
}

export default Message