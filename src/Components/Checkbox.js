
function Checkbox (props) {
  console.log(props)
    return (
        <div className='layout'>
      <img src='https://images.unsplash.com/photo-1504512925757-05a29947cfdd?ixlib=rb-4.0.3&
      ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTF8fGJsdWUlMjBmbG93ZXJzfGVufDB8fDB8fA%3D%3D&auto=
      format&fit=crop&w=500&q=60'></img>
      <div>
      <div className='header-content'>
        
        <div className='header-status'>
          <p className='box'>{props.data['signupPage']}</p>
          <p>Sign Up</p>
        </div>
        <div className='header-status'>
          <p className='box'>{props.data['messagePage']}</p>
          <p>Message</p>
        </div>
        <div className='header-status'>
          <p className='box'>{props.data['checkboxPage']}</p>
          <p>Checkbox</p>
        </div>
        
      </div> 
        <hr></hr>
        <div className='checkbox'>
            <p>Step 3/3</p>
            <h1>Checkbox</h1>
            <div className="image-container">
              <div className="girl-container" id = {props.data['comp-opt-one'] ? 'active' : 'non-active'}> 
                <label htmlFor="comp-opt-one" ><img className = 'girl' src ='https://res.cloudinary.com/dbpcftrzu/image/upload/v1672204889/f108eba3bfb6dc43be861b3fd697eceb-removebg-preview_kskgqz.png' /></label>
                <input type= 'radio' id ="comp-opt-one" name='select' onChange = {props.updater} checked = {props.data['comp-opt-one']}></input>
              </div>
              <div className="boy-container" id = {props.data['comp-opt-two'] ? 'active' : 'non-active'}> 
                <label htmlFor="comp-opt-two"><img className = 'boy' src='https://res.cloudinary.com/dbpcftrzu/image/upload/v1672205066/db68c39d7b021f24a67e06b154771ebc-removebg-preview_2_yjbjob.png'/></label>
                <input type='radio' id="comp-opt-two" name='select' onChange = {props.updater} checked={props.data['comp-opt-two']}></input>
              </div>
            
            </div>
            <p className="error">{props.data['error']['comp-opt']}</p>
            <div className="second-container">
            <div>
              <input type= 'radio' id='second-one' name = 'second' onChange = {props.updater} checked={props.data['second-one']}></input>
              <label htmlFor="second-one">I want to add this option</label>
              
            </div>
            <div>
              <input type= 'radio' id='second-two' name='second' onChange = {props.updater} checked={props.data['second-two']}></input>
              <label htmlFor="second-two">Let me click on this to explore more options</label>
            </div>
            </div>
            <p className="error">{props.data['error']['second']}</p>
        <hr></hr>
        <br></br>
        <br></br>
        <div className="message-footer">
            <div className="message-back">
                <button onClick = {props.back}>Back</button>
            </div>
            <div className='message-next'> 
                <button onClick = {props.submit}>Submit</button>
            </div>
            
        </div>
        
      </div>
    </div>
    </div>
    )
}

export default Checkbox