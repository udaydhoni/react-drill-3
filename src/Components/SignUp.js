
function SignUp (props) {
  console.log(props.error)
    return (
        <div className='layout'>
      <img src='https://images.unsplash.com/photo-1504512925757-05a29947cfdd?ixlib=rb-4.0.3&
      ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTF8fGJsdWUlMjBmbG93ZXJzfGVufDB8fDB8fA%3D%3D&auto=
      format&fit=crop&w=500&q=60' className="opening-page"></img>
      <div>
      <div className='header-content'>
        
        <div className='header-status'>
          <p className='box'>{props['totalData']['signupPage']}</p>
          <p>Sign Up</p>
        </div>
        <div className='header-status'>
          <p className='box'>{props['totalData']['messagePage']}</p>
          <p>Message</p>
        </div>
        <div className='header-status'>
          <p className='box'>{props['totalData']['checkboxPage']}</p>
          <p>Checkbox</p>
        </div>
        
      </div> 
        <hr></hr>
        <div className='actual-sign-up'>
            <p>Step 1/3</p>
            <h1>Sign UP</h1>



            <div className='name-details'>
            <div className="first-name">
                <label for = 'first-name'>First name</label>
                <br></br>
                <br></br>
                <input type='text' id='first-name' onChange= {props.updater} value={props.data['first-name']}></input>
                <p className="error">{props['error']['first-name']}</p>
            </div>
            <br></br>
            <br></br>
            <div className="last-name">
                
                <label for = 'last-name'>Last name</label>
                <br></br>
                <br></br>
                <input type='text' id ='last-name' onChange={props.updater} value={props.data['last-name']}></input>
                <p className="error">{props['error']['last-name']}</p>
            </div>
            </div>


            <br></br>
            <br></br>
            <div className='second-field'>
            <div className="dob">
                <label for = 'dob'>Date of Birth</label>
                <br></br>
                <br></br>
                <input type='date' id='dob' onChange={props.updater} value={props.data['dob']}></input>
                <p className="error">{props['error']['dob']}</p>
            </div>
            <br></br>
            <br></br>
            <div className="email">
                <label for = 'email'>Email Address</label>
                <br></br>
                <br></br>
                <input type='text' id='email' onChange={props.updater} value={props.data['email']}></input>
                <p className="error">{props['error']['email']}</p>
            </div>
            </div>
            <br></br>
            <br></br>
            <div>
              <label for='address'>Address</label>
              <br></br>
              <br></br>
              <input type='text' id= 'address' onChange={props.updater} value={props.data['address']}></input>
              <p className="error">{props['error']['address']}</p>
            </div>


        </div>
        <br></br>
        <br></br>
        <hr></hr>
        <br></br>
        <br></br>
        <div className='next'> 
        <button onClick={props.next}>Next Step</button>
        </div>
      </div>
    </div>
    )
}

export default SignUp