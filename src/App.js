import logo from './logo.svg';
import './App.css';
import SignUp from './Components/SignUp';
import Message from './Components/Message';
import Checkbox  from './Components/Checkbox';
import { useState } from 'react';

function App() {
  let [page,setPage] = useState(0);
  let [formData, setFormData] = useState({
    signupPage:1,
    messagePage:2,
    checkboxPage:3,
    firstComponent:{
      'first-name':'',
      'last-name':'',
      dob: '',
      email:'',
      address: ''
    },
    error: {
      'first-name':'',
      'last-name':'',
      dob: '',
      email:'',
      address: '',
      message: '',
      radio:'',
      'comp-opt': '',
      'second': '',
    },
    message: '',
    'first-option': false,
    'second-option': false,
    'comp-opt-one': '',
    'comp-opt-two': '',
    'second-one': '',
    'second-two':''

  })

  function changeSignup () {
    formData['signupPage'] = '✓'
  }
  function changeMessage () {
    formData['messagePage']= '✓'
  }
  function changeCheckbox () {
    formData['checkboxPage']= '✓'
  }
  function nextHandler () {
    let noError = true;
    if (page === 0) {
      for (let keys in formData['firstComponent']) {

        if (!formData['firstComponent'][keys]) {
          noError = false
          setFormData((prevState) => {
            prevState['error'][keys] = `${keys}` + ' cannot be empty'
            return {
              ...prevState
            }
          })
        }
      }
    } else if (page === 1) {
        if (formData['message'].length > 200 || formData['message'].length <2) {
          noError = false
          setFormData((prevState)=>{
            prevState['error']['message'] = 'Message length must be between 2-200 characters'
            console.log(prevState)
            return {
              ...prevState
            } 
          }) 
        }
         if (!(formData['first-option'] || formData['second-option'])) {
          noError =false
          setFormData((prevState)=>{
            prevState['error']['radio'] = 'Please select one option'
            console.log(prevState)
            return {
              ...prevState
            } 
          })
        }
    } 

      if (noError) {
        setPage((prevState)=>{
          if (prevState === 0) {
            changeSignup()
          } else if (prevState === 1) {
            changeMessage()
          } else {
            changeCheckbox()
          }
          return prevState+1})
      } else {
        setPage((prevState) => {return prevState})
      }
    }

  
  function backHandler () {
    if (page === 1) {
       setPage((prevState)=>{
        console.log(prevState)
        return prevState-1})
    } else if (page === 2) {
       setPage((prevState)=>{
        console.log(prevState)
        return 1})
    } 
  }

  function submitHandler () {
    let noError = true
    if (!(formData['comp-opt-one'] || formData['comp-opt-two'])) {
      noError = false
      setFormData((prevState) => {
        prevState['error']['comp-opt'] = 'please select atleast one option'
        return {
          ...prevState
        }
      })
    }
    if (!(formData['second-one'] || formData['second-two'])) {
      noError = false
      setFormData((prevState) => {
        prevState['error']['second'] = 'please select atleast one option'
        return {
          ...prevState
        }
      })
    }
    if (noError) {
      setPage((prevState) => {
        console.log(prevState)
        setFormData((prevState) => {
          changeCheckbox()
          return {
            ...prevState
          }
        })
        return 2
      })
      alert('Your form has been submitted')
    }
  }

  function updateFirstComponentData (event) {
    let key = event.target.id
    console.log(formData)
    setFormData((prevState)=>{
      prevState['firstComponent'][key] = event.target.value
      prevState['error'][key] = ''
      return {
        ...prevState
      }
    })
  }
  function updateData (event) {
    let key = event.target.id
    setFormData((prevState) => {
      console.log(formData)
      if (event.target.type !== 'radio') {
        prevState[key] = event.target.value
        if (event.target.value.length > 2) {
          prevState['error']['message'] = ''
        }
      } else {
        if (key === 'first-option') {
          prevState['first-option'] = true
          prevState['second-option'] = false
          prevState['error']['radio'] =''
        } else {
          prevState['second-option'] = true
          prevState['first-option'] = false
          prevState['error']['radio'] =''
        }
      }
      
      return {
        ...prevState
      }
    })
  }

  function updateThirdComponentData (event) {
    let key = event.target.id
    setFormData((prevState)=>{
      console.log(prevState)
      if (key === 'comp-opt-one') {
        prevState['comp-opt-one'] = true
        prevState['comp-opt-two'] = false
        prevState['error']['comp-opt'] = ''
      } else if (key === 'comp-opt-two'){
        prevState['comp-opt-one'] = false
        prevState['comp-opt-two'] = true
        prevState['error']['comp-opt'] = ''
      }
      if (key === 'second-one') {
        prevState['second-one'] = true
        prevState['second-two'] = false
        prevState['error']['second'] = ''
      } else if (key === 'second-two') {
        prevState['second-one'] = false
        prevState['second-two'] = true
        prevState['error']['second'] = ''
      }
      return {
        ...prevState
      }
    })
  }
  function display () {
    console.log(formData)
    if (page === 0) {
      return <SignUp next= {nextHandler} data = {formData['firstComponent']} totalData = {formData} error = {formData['error']} updater={updateFirstComponentData}></SignUp>
    } else if (page === 1) {
      return <Message next = {nextHandler} back={backHandler} data={formData} updater={updateData}></Message>
    } else {
      return <Checkbox submit = {submitHandler} back={backHandler} data={formData} updater={updateThirdComponentData}></Checkbox>
    }
  }
  return (
    <div className="form">
      {display()}
    </div>
  );
}

export default App;
